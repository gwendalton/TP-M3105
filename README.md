# Projet POO 3A 2017
Le projet a pour but de créer une librairie pour historiser des évènements et pour tester du code, dans le cadre d'une application de gestion de compte
bancaire. Ainsi, l'utilisateur effectuera ses transactions normalement, et l'intérêt sera garder une trace de tout ce qu'il fait et de tester 
l'efficacité de notre code.

## Membres du projet
- MANORANJITHAN Ashanth
- PAPAIL Gwendal
- PATEL Safwaane


## Compilation
Le fichier compile.sh lancera la compilation de tous les fichiers java des différents projets.
Le fichier test.sh lancera les tests du projet banking.
Le fichier launch_banking.sh lancera l'application banking.

## Présentation des différentes parties

### Banking
Le Banking constitue l'application de gestion de compte bancaire. Ainsi, via le terminal, on peut afficher les opérations disponibles, créer un compte,
faire un dépôt ou un retrait d'argent, etc.


### Logger
Le Logger recense l'intégralité des évènements du Banking dans un fichier texte.


### Librairie de Tests
Le fichier Runtest va afficher dans la console des informations sur l'efficacité du programme, telles que le statut, la durée d'exécution,
le pourcentage de réussite, etc.


